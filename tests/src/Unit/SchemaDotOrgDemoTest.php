<?php

declare(strict_types=1);

namespace Drupal\Tests\schemadotorg_demo\Unit;

use Drupal\Tests\UnitTestCase;

/**
 * Test to trigger Drupal GitLab CI pipeline.
 *
 * @group schemadotorg
 */
class SchemaDotOrgDemoTest extends UnitTestCase {

  /**
   * Test to trigger Drupal GitLab CI pipeline.
   */
  public function test(): void {
    $this->assertTrue(TRUE);
  }

}
