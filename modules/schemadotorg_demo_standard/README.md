Table of contents
-----------------

* Introduction
* Features
* Requirements
* Notes


Introduction
------------

The **Schema.org Blueprint Demo Standard module** provides an opinionated demo of
the Schema.org Blueprints built on top of Drupal's standard profile.

**THIS MODULE SHOULD ONLY BE INSTALLED ON A PLAIN VANILLA STANDARD
INSTANCE OF DRUPAL**


Features
--------

Installation

- Installs contributed module settings. (/config/install)
- Rewrites contributed module settings. (/config/rewrite)
- Adds a Statement content block to the front page.
- Uninstalls the Comment module and its associated fields.
- Removes the 'Type tray' module's default link text to view existing nodes on each content type.

Contrib

- **Entity Print:** Styles entity print links as buttons.
- **Entity browser:** Removes types that are contextually disallowed by the View.
