Table of contents
-----------------

* Introduction
* Features


Introduction
------------

The **Schema.org Blueprint Demo Admin module** enables and configures
API support for the Schema.org Blueprints Demo.


Features
--------

- Adds consumer image styles to the 'Default Consumer'.
- Adds shortcuts linking to key Schema.org Blueprints URLs.
- Creates path aliases for /api/*.
- Hides the API version from JSON:API.
