Table of contents
-----------------

* Introduction
* Features


Introduction
------------

The **Schema.org Blueprint Demo Standard Translation module** provides 
translation support for the Schema.org Blueprints demo.


Features
--------

- Installs interface, configuration, and content translation support.
- Enables Spanish support
- Enables path prefix language negotiation. (i.e. /es/*)
- Enable content translation redirects for content entities.

