Table of contents
-----------------

* Introduction


Introduction
------------

The **Schema.org Blueprint Demo Admin module** enables and configures
development tools for the Schema.org Blueprints Demo.
